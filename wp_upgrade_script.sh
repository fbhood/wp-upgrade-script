#!/bin/bash

line="_________________________________________________________________________"

check_maintenance(){
    wp maintenance-mode is-active
    if [ $? != 0 ]; then 
        wp maintenance-mode activate
        echo "Maintenance mode Enabled successfully"
    else
        echo "Maintenance mode was already enabled"
    fi
}

echo $line
echo "Enabling Maintenance Mode"
check_maintenance
echo $line

echo $line
echo "checking core updates"
wp core check-update
echo $line

read -p "Input the version you want to update to: [5.4.2:to update - s:to skip] " wp_version

if [ "$wp_version" = "s" ]; then
        echo "Skipping core update"

else
    wp core update --version=$wp_version
fi
echo $line

echo "Updating all Plugins"
wp plugin update --all
echo "Done"
echo $line

echo $line
echo "Updating WordPress db"
wp core update-db
echo "Done."
echo $line


echo $line
echo "Updating WC database"
wp wc update
echo "Done."
echo $line

echo "All tasks completed!"
wp maintenance-mode deactivate
