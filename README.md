# WordPress update bash Script

This script aims to automate a series of tasks required to update wordpress
core, db and plugins.

## Run the script

```bash

./wp_upgrade_script.sh
```

## Tasks automated

List of all wp-cli commands automatically executed by this script.

- wp maintenance-mode is-active
- wp maintenance-mode activate
- wp core update-check
- wp core update (prompt to instert version)
- wp plugin update --all
- wp core update-db
- wp wc update
- wp maintenance-mode deactivate
